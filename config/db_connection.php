<?php 
header('Content-Type: text/html; charset=utf-8');
$tablename = $_POST["tablename"];

 if (!isset($_COOKIE["tablename"]) ) {
    setcookie( "tablename", $tablename, strtotime( '+1 year' ), '/' );
    
 if (!isset($_POST["tablename"])) {
      $tablename = $_COOKIE["tablename"];
 }; 
 }
 
function checkConnection()
{
    $servername = $_POST["servername"];
    $dbusername = $_POST["dbusername"];
    $dbpassword = $_POST["dbpassword"];
    $mydb = $_POST["mydb"] ;
    
  if (!isset($_COOKIE["servername"]) && !isset($_COOKIE["dbusername"]) && !isset($_COOKIE["dbpassword"]) && !isset($_COOKIE["mydb"])) {
    setcookie( "servername", $servername, strtotime( '+1 year' ), '/' );
    setcookie( "dbusername", $dbusername, strtotime( '+1 year' ), '/' );
    setcookie( "dbpassword", $dbpassword, strtotime( '+1 year' ), '/' );
    setcookie( "mydb", $mydb, strtotime( '+1 year' ), '/' );
   };
   
  if (!isset($_POST["servername"])) {
      $servername = $_COOKIE["servername"];
  } elseif (!isset($_POST["dbusername"])) {
      $dbusername = $_COOKIE["dbusername"];
  } elseif (!isset($_POST["dbpassword"])) {
      $dbpassword = $_COOKIE["dbpassword"];
  } elseif (!isset($_POST["mydb"])) {
      $mydb = $_COOKIE["mydb"];
  }; 
  
   define('HOST', $servername); // Database host name ex. localhost
        define('USER', $dbusername); // Database user. ex. root ( if your on local server)
        define('dbpassword', $dbpassword); // Database user dbpassword  (if dbpassword is not set for user then keep it empty )
        define('DATABASE', $mydb); // Database name
        define('CHARSET', 'utf8');
  
  if (isset($_POST["servername"]) && isset($_POST["dbusername"]) && isset($_POST["dbpassword"]) && isset($_POST["mydb"])  ) {
     
     try {
                    $conn = new PDO("mysql:host=$servername;dbname=$mydb", $dbusername, $dbpassword);
                    // set the PDO error mode to exception
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    ?>
                    <div class="alert alert-success" role="alert">
                      <h2 class="alert-heading">Minden rendben! A kapcsolat létrejött.</h2>
                      <p class="mb-0">Most 10 másodpercen belül átirányítjuk az előző oldalra, ahol létrehozhatja az adatbázistáblát is!</p>
                    </div>
                    <?php
                    header( "refresh:10;url= ../first-time.php" );
                    exit();
                   // header( "refresh:10;url= ../index.php" );
                   // exit();
          } catch(PDOException $e) {
                    ?>
                     <div class="alert alert-danger" role="alert">
                      <h2 class="alert-heading">Baj van! A kapcsolat létrehozása sikertelen.</h2>
                      <p class="mb-0">Most 10 másodpercen belül átirányítjuk az előző oldra, ahol újra próbálkozhat a kapcsolat helyes beállításával!</p>
                    </div>
                    <?php
                    header( "refresh:10;url= ../first-time.php" );
                    exit();
                    $e->getMessage();
          }
  }; 
}

function DB()
{
   checkConnection();
   
    static $instance;
    if ($instance === null) {
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => FALSE,
        );
        $dsn = 'mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=' . CHARSET;
         array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET time_zone = \'+01:00\'');
      
    }
    return $instance;

}

function createTable()
{
$tablename = $_POST["tablename"] ;
$servername = $_COOKIE["servername"];
$dbusername = $_COOKIE["dbusername"];
$dbpassword = $_COOKIE["dbpassword"];
$mydb = $_COOKIE["mydb"];

    
    //Connect to the database on cloud9
    $host = $servername;
    $user = $dbusername;                     //Your Cloud 9 username
    $pass = $dbpassword;                                  //Remember, there is NO password by default!
    $mydb = $mydb;                                  //Your database name you want to connect to
    $port = 3306;
    
    $connection = mysqli_connect($host, $user, $pass, $mydb, $port)or die(mysql_error());



    //And now to perform a simple query to make sure it's working
    $query = "CREATE table IF NOT EXISTS $tablename (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(50) NOT NULL,
                          `username` varchar(50) NOT NULL,
                          `email` varchar(100) NOT NULL,
                          `password` varchar(250) NOT NULL,
                          `google_secret_code` varchar(250) NOT NULL,
                          `is_admin` varchar(20) NOT NULL DEFAULT 'undefined',
                          `is_banned` tinyint (1) DEFAULT '0',
                          `created_at` TIMESTAMP DEFAULT NOW(),
                          `login_at` TIMESTAMP,
                          PRIMARY KEY (`id`)
                        );";
    $result = mysqli_query($connection, $query);
    if (isset($result) && isset($tablename)){
        ?>
        <div class="alert alert-success" role="alert">
            <h2 class="alert-heading">Minden rendben! Az adattábla létrejött.</h2>
            <p class="mb-0">Most 10 másodpercen belül átirányítjuk egy bejelentkező oldalra, ahol létrehozhatja első regisztrációját!</p>
        </div>
        <?php
        header( "refresh:10;url= ../index.php" );
        exit();
    }
}

?>

<!doctype html>
<html lang="hu">
<head>
    <title>Adatbázis kapcsolat létrehozása</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</head>
<body>
  <container>
      <div class="row">
        <div class="col-md-12">
            <h4 style="text-align: center">
                <?php
                    checkConnection();
                    if (isset($tablename)) {
                        createTable();
                    };
                ?>
            </h4>
        </div>
     </div>
  </container>
</body>
</html>
