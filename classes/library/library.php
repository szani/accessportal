<?php

/*
 * Tutorial: Google Multi factor authentication in PHP
 *
 * Page: Application library
 * */
 
define('TIMEZONE', 'Europe/Budapest');
date_default_timezone_set(TIMEZONE);

class DemoLib
{

    public $db;
    
    function __construct($db)
    {
        if (isset($_COOKIE["servername"]) && isset($_COOKIE["dbusername"]) && isset($_COOKIE["dbpassword"]) && isset($_COOKIE["mydb"]) && isset ($_COOKIE["tablename"]) ) {
             
            $servername = $_COOKIE["servername"];
            $dbusername = $_COOKIE["dbusername"];
            $dbpassword = $_COOKIE["dbpassword"];
            $mydb = $_COOKIE["mydb"];

            $tablename = $_COOKIE["tablename"]; 
            // timezone beállítása gmt szerint
            // http://www.sitepoint.com/synchronize-php-mysql-timezone-configuration/
            $now = new DateTime();
            $mins = $now->getOffset() / 60;
            $sgn = ($mins < 0 ? -1 : 1);
            $mins = abs($mins);
            $hrs = floor($mins / 60);
            $mins -= $hrs * 60;
            $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
            
            try{
                 $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                 $this->db=new PDO('mysql:host='.$dbservername.';charset=utf8;dbname='.$mydb, $dbusername, $dbpassword, $pdo_options);
                 
                 $this->db->exec("SET time_zone='$offset';");
            }
            catch(Exception $e)
                 {
                   die('Hiba: '.$e->getMessage());
            }
         } else {
             $this->db = $db;
         };
         
    }

    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Register New User
     *
     * @param $name, $email, $username, $password, $auth_code, $created_at
     * @return ID
     * */
    public function Register($name, $email, $username, $password, $google_secret_code, $created_at)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("INSERT INTO $mydb.$tablename (name, email, username, password, google_secret_code, created_at) VALUES (:name,:email,:username,:password,:google_secret_code,:created_at)");
        $query->bindParam("name", $name, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("username", $username, PDO::PARAM_STR);
        // $enc_password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 11]);
        $hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);
        $query->bindParam("password", $hash, PDO::PARAM_STR);
        $query->bindParam("google_secret_code", $google_secret_code, PDO::PARAM_STR);
        $query->bindParam("created_at", $created_at, PDO::PARAM_STR);
        $query->execute();
        return $this->db->lastInsertId();
    }

    /*
     * Check Username
     *
     * @param $username
     * @return boolean
     * */
    public function isUsername($username)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT id FROM $mydb.$tablename WHERE username=:username");
        $query->bindParam("username", $username, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Check Email
     *
     * @param $email
     * @return boolean
     * */
    public function isEmail($email)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT id FROM $mydb.$tablename WHERE email=:email");
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() > 0) {
            return true;
        } else {
            return false;
        }

    }
    
    
    /* Login
     *
     * @param $username, $password
     * @return $mixed
     * */
   public function Login($username, $password)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT id, password FROM $mydb.$tablename WHERE username=:username OR email=:email");
        $query->bindParam("username", $username, PDO::PARAM_STR);
        $query->bindParam("email", $username, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() > 0) {
            $result = $query->fetch(PDO::FETCH_OBJ);
            $enc_password = $result->password;
            if (password_verify($password, $enc_password)) {
                return $result->id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
     /* LoginTime 
     *
     * @param $login_at
     * @return ID
     * */
    public function LoginTime($login_at,$id)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("UPDATE $mydb.$tablename SET `login_at` = :login_at WHERE `id`=:id");
        $query->bindParam("login_at", $login_at, PDO::PARAM_STR);
        $query->bindParam("id", $id, PDO::PARAM_STR);
       // $query->bindParam("id", $id, PDO::PARAM_STR);
        $query->execute($user_id);
        return $this->db->lastInsertId();
    }

    /*
     * get User Details
     *
     * @param $user_id
     * @return $mixed
     * */
   public  function UserDetails($user_id)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT id, name, username, email, google_secret_code, created_at, login_at, is_admin, is_banned FROM $mydb.$tablename WHERE id=:user_id");
        $query->bindParam("user_id", $user_id, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() > 0) {
            return $query->fetch(PDO::FETCH_OBJ);
        }
    }
       
}

