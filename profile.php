<?php

define('TIMEZONE', 'Europe/Budapest');
date_default_timezone_set(TIMEZONE);
// Start Session
session_start();
$role = $_SESSION['sess_userrole'];
    if(!isset($_SESSION['sess_username']) || $role!="user"){
      header('Location: index.php?err=2');
    }

// check user login
if(empty($_SESSION['user_id']))
{
    header("Location: index.php");
}

// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);
$user = $app->UserDetails($_SESSION['user_id']);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Felhasználói profil</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="template/css/bootstrap.min.css">
    <!-- jQuery and Bootstrap scripts -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- Clock -->
    <script type="text/javascript" src="js/clock.js"></script>
<style>
    .table-borderless > tbody > tr > td,
    .table-borderless > tbody > tr > th,
    .table-borderless > tfoot > tr > td,
    .table-borderless > tfoot > tr > th,
    .table-borderless > thead > tr > td,
    .table-borderless > thead > tr > th {
        border: none;
        
    }
    .table {
        max-width: 400px;
    }
</style>
</head>
<body>

<div class="container">
   <div class="row jumbotron">
        <div class="col-md-12">
            <h1 style="text-align: center">
                Felhasználói profil oldal
            </h1>
        </div>
  </div>
  <h4>Üdvözlöm <span style="color: #007bff;"><?php echo $user->name; ?></span>! <br>A pontos idő: <span class="clock"></span> </h4>
  <table style="width: 100%; margin: 0 auto;" class="table table-borderless">
    <div style="max-width: 350px;"><class="table-responsive"> 
    <thead>
      <tr>
        <th>Az Ön adatai:</th>
        <br>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>Név:</th>
        <td><?php echo $user->name; ?></td>
      </tr>
      <tr>
        <th>Felhasználónév:</th>
        <td><?php echo $user->username; ?></td>
      </tr>
      <tr>
        <th>Email:</th>
        <td><?php echo $user->email; ?></td>
      </tr>
    </tbody>
     </div>
  </table>
  <br><br>
  <p style="text-align: right"><a class="btn btn-primary" href="logout.php" role="button">Kilépés</a></p>
</div>
</body>
</html>

