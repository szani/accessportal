<?php

define('TIMEZONE', 'Europe/Budapest');
date_default_timezone_set(TIMEZONE);

// Start Session
    session_start();
    $role = $_SESSION['sess_userrole'];
    if(empty($_SESSION['sess_username'] || $role!="admin" || $role!="user" || $role!="undefined")){
      header('Location: index.php?err=2');
    }

// check user login
if(empty($_SESSION['user_id']))
{
    header("Location: index.php");
}

// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);
$user = $app->UserDetails($_SESSION['user_id']);
$id = $user->id;
$is_admin = $_POST[$is_admin];
// 

require __DIR__ . '/classes/ajax/lib.php';
$db = DB();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Adminisztrátor profil</title>
     <!-- jQuery -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="template/css/bootstrap.min.css">
    <!-- jQuery and Bootstrap scripts -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- Clock -->
    <script type="text/javascript" src="js/clock.js"></script>
    <!-- Custom JS file -->
    <script type="text/javascript" src="js/script.js"></script>
  
<style>
    .table-borderless > tbody > tr > td,
    .table-borderless > tbody > tr > th,
    .table-borderless > tfoot > tr > td,
    .table-borderless > tfoot > tr > th,
    .table-borderless > thead > tr > td,
    .table-borderless > thead > tr > th {
        border: none;
    }
    #table_profil {
        max-width: 400px;
    }
     #table_records {
        max-width: 800px;
    }
    .records_content {
        display:none;
    }
</style>
</head>
<body>

<div class="container">
   <div class="row jumbotron">
        <div class="col-md-12">
            <h1 style="text-align: center">
                Adminisztrátor profil oldal
            </h1>
        </div>
  </div>
  <h4>Üdvözlöm <span style="color: #007bff;"><?php echo $user->name; ?></span>! <br>A pontos idő: <span class="clock"></span> </h4>
  <table style="width: 100%; margin: 0 auto;" class="table table-borderless" id="table_profil">
    <div style="max-width: 350px;"><class="table-responsive"> 
    <thead>
      <tr>
        <th>Az Ön adatai:</th>
        <br>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>Név:</th>
        <td><?php echo $user->name; ?></td>
      </tr>
      <tr>
        <th>Felhasználónév:</th>
        <td><?php echo $user->username; ?></td>
      </tr>
      <tr>
        <th>Email:</th>
        <td><?php echo $user->email; ?></td>
      </tr>
    </tbody>

     
     </div>
     
  </table>

  <br><br>
  <p style="text-align: right"><a class="btn btn-primary" href="logout.php" role="button">Kilépés</a></p>
   <hr>
 
</div>

  <div class="panel panel-default">
    <!-- Default panel contents -->
    <!-- Content Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Regisztrált felhasználók</h1>
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
               <!-- <h3>Lista:</h3> -->
                <br>
                <div style="text-align: center;" class="records_content"></div>
                 <table style="width: 100%; margin: 0 auto;" class="table table-borderless" id="table_profil">
                    <div> <class="table-responsive"></div>
                 </table>
                <!-- <button id="btn-1" class="btn btn-primary">Elrejt</button> -->
                <button id="btn" class="btn btn-primary">Lista</button>
            </div>
        </div>
    </div>
    <!-- /Content Section -->
    <!-- Bootstrap Modals -->
    <!-- Modal - Add New Record/User -->
   <!--
    <div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Record</h4>
                </div>
                <div class="modal-body">
    
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" placeholder="First Name" class="form-control"/>
                    </div>
    
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" placeholder="Last Name" class="form-control"/>
                    </div>
    
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="text" id="email" placeholder="Email Address" class="form-control"/>
                    </div>
    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="addRecord()">Add Record</button>
                </div>
            </div>
        </div>
    </div>
   -->
    <!-- // Modal -->
  
<!-- // Modal -->
    <!-- Modal - Update User details -->
    <div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <form method="post" id="user_form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Módosítás</h4>
                </div>
                <div class="modal-body">
    
                    <div class="form-group">
                        <label for="update_name">Név</label>
                        <input type="text" id="update_name" placeholder="Név" class="form-control"/>
                    </div>
    
                    <div class="form-group">
                        <label for="update_username">Felhasználónév</label>
                        <input type="text" id="update_username" placeholder="Felhasználónév" class="form-control"/>
                    </div>
    
                    <div class="form-group">
                        <label  for="update_email">Email </label>
                        <input type="text" id="update_email" placeholder="Email " class="form-control"/>
                    </div>
                    
                     <div class="form-group">
                        <label  for="update_radio">Jogosultság</label>
                           <p><label><input name="is_admin" type="radio" value="user"  class="is_admin" /> Felhasználó</label></p>
                           <p><label><input name="is_admin" type="radio" value="admin" class="is_admin" /> Adminisztrátor</label></p>
                    </div>
                    
                    <div class="form-group">
                        <label  for="update_radio">Tiltás</label>
                           <p><label><input name="is_banned" type="radio" value="1"  class="is_banned" /> Eltiltva</label></p>
                           <p><label><input name="is_banned" type="radio" value="0" class="is_banned" /> Nincs eltiltva</label></p>
                    </div>
                
                <div class="modal-footer">
                    <input type="hidden" id="check7" value="user_form" name="check7" />
                    <button type="submit" class="btn btn-primary" value="save" onclick="UpdateUserDetails();" >Változtatások mentése</button>
                    <input type="hidden" id="hidden_user_id">
                </div>
            </div>
            </div>
            <div class="privacy_info"></div>
          </form>
        </div>

    </div>
    <!-- // Modal -->
  </div>
</body>
</html>
<script>

$(document).ready(function()
{
    $("#btn").click(function()
    {
        $(".records_content").show();
    });
});


</script>