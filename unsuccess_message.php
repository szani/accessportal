<head>
    <title>Adatbázis kapcsolat létrehozása</title>
    <!-- Latest compiled and minified CSS -->
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 style="text-align: center">
                <?php
                    ?>
                     <div class="alert alert-danger" role="alert">
                      <h2 class="alert-heading">Van baj! A kapcsolat létrehozása sikertelen.</h2>
                      <p class="mb-0">Most 10 másodpercen belül átirányítjuk az előző oldra, ahol újra próbálkozhat a kapcsolat helyes beállításával!</p>
                    </div>
                    <?php
                    header( "refresh:10;url= first-time.php" );
                    exit();
                 ?>
            </h1>
        </div>
    </div>
   
</div>
</body>
<script>
  //Registration button
  
$(document).ready(function() {
$('#btn-next').on('click', function() {
$('#loginbox').hide(); $('#1box').show();
   })
});
</script>
 