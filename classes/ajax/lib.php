<?php


class CRUD
{

    public $db;

      function __construct()
    {
         if (isset($_COOKIE["servername"]) && isset($_COOKIE["dbusername"]) && isset($_COOKIE["dbpassword"]) && isset($_COOKIE["mydb"]) && isset ($_COOKIE["tablename"]) ) {
             
              
            $servername = $_COOKIE["servername"];
            $dbusername = $_COOKIE["dbusername"];
            $dbpassword = $_COOKIE["dbpassword"];
            $mydb = $_COOKIE["mydb"];
            $port = 3306;
            $tablename = $_COOKIE["tablename"]; 
            try{
                 $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                 $this->db=new PDO('mysql:host='.$dbservername.';charset=utf8;dbname='.$mydb, $dbusername, $dbpassword, $pdo_options);
            }
            catch(Exception $e)
                 {
                   die('Hiba: '.$e->getMessage());
            }
         } else {
             $this->db = $db;
         };
         
    }
    
    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Add new Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * 
    public function Create($first_name, $last_name, $email)
    {
        $query = $this->db->prepare("INSERT INTO users(first_name, last_name, email) VALUES (:first_name,:last_name,:email)");
        $query->bindParam("first_name", $first_name, PDO::PARAM_STR);
        $query->bindParam("last_name", $last_name, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->execute();
        return $this->db->lastInsertId();
    }

    /*
     * Read all records
     *
     * @return $mixed
     * */
    public function Read()
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT * FROM $tablename;");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /*
     * Delete Record
     *
     * @param $user_id
     * */
    public function Delete($user_id)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("DELETE FROM $tablename WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
    }

    /*
     * Update Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */
 public function Update($name, $username, $email, $user_id, $is_admin, $is_banned)
    {
        $tablename = $_COOKIE["tablename"];
        $mydb = $_COOKIE["mydb"];
        $query = $this->db->prepare("UPDATE $mydb.$tablename SET name = :name, username = :username, email = :email, is_admin = :is_admin, is_banned = :is_banned  WHERE id = :id");
        $query->bindParam("name", $name, PDO::PARAM_STR);
        $query->bindParam("username", $username, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("is_admin", $is_admin, PDO::PARAM_STR);
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->bindParam("is_banned", $is_banned, PDO::PARAM_INT);
        $query->execute();
    }

    /*
     * Get Details
     *
     * @param $user_id
     * */
    public function Details($user_id)
    {
        $tablename = $_COOKIE["tablename"]; 
        $query = $this->db->prepare("SELECT * FROM $tablename WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }
    
     /* isAdmin 
     *
     * @param $is_admin
     * @return ID
     * */
    public function IsAdmin($is_admin,$id,$is_banned)
    {
       
        $tablename = $_COOKIE["tablename"]; 
        $mydb = $_COOKIE["mydb"];
        
        $query = $this->db->prepare("UPDATE $mydb.$tablename  SET is_admin = :is_admin, is_banned = :is_banned WHERE id=:id");
        $query->bindParam("is_admin", $is_admin, PDO::PARAM_STR);
        $query->bindParam("id", $id, PDO::PARAM_STR);
        $query->bindParam("is_banned", $is_banned, PDO::PARAM_INT);
       // $query->bindParam("id", $id, PDO::PARAM_STR);
        $query->execute($is_admin,$is_banned,$id);
        return $this->db->lastInsertId();
    }
}

?>
