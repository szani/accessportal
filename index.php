<?php

//check and set cookie FirstTimer

  if (!isset($_COOKIE["FirstTimer"])){
    setcookie( "FirstTimer", 1, strtotime( '+1 year' ) );
    header("Location: first-time.php");
    exit;
 
   };
   
// Set Timezone
 define('TIMEZONE', 'Europe/Budapest');
 date_default_timezone_set(TIMEZONE);
// Start Session
session_start();
                        
// Database connection
require __DIR__ . '/config/db_connection.php';
 $db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);
$user = $app -> UserDetails($_SESSION['user_id']);
$is_banned =  $_SESSION['sess_is_banned'];

$login_error_message = '';

// check Login request
if (!empty($_POST['btnLogin'])) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $is_banned =  $_SESSION['sess_is_banned']; 

    if ($username == "") {
        $login_error_message = 'Felhasználónév kötelező!';
    } else if ($password == "") {
        $login_error_message = 'Jelszó kötelező!';
    } else if ($is_banned == 1) {
        $login_error_message = 'Tiltott felhasználó!';
    }else {
        $user_id = $app->Login($username, $password); // check user login
        
        if($user_id > 0)
        {
            $_SESSION['user_id'] = $user_id; // Set Session
            header("Location: validate_login.php"); // Redirect user to validate auth code
        }
        else
        {
            $login_error_message = 'Érvénytelen bejelentkezési adatok!';
        }
    }
}

//errors

 $errors = array(
 1=>"Érvénytelen felhasználónév, vagy jelszó! Próbálkozzon újra!",
 2=>"Kérem jelentkezzen be, hogy hozzáférjen az oldal tartalmához!",
 3=>"Ki van tiltva az oldalról, vegye fel a kapcsolatot az adminisztrátorral!"
 );

 $error_id = isset($_GET['err']) ? (int)$_GET['err'] : 0;

 if ($error_id == 1) {
 echo '<p class="text-danger">'.$errors[$error_id].'</p>';
 }elseif ($error_id == 2) {
 echo '<p class="text-danger">'.$errors[$error_id].'</p>';
 }elseif ($error_id == 3) {
 echo '<p class="text-danger">'.$errors[$error_id].'</p>';
 }

?>

<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Belépés</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="template/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <div class="row jumbotron">
        <div class="col-md-12">
             <h1 style="text-align: center">
                Belépőrendszer kétlépcsős Google hitelesítéssel 
             </h1>
        
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 well">
            <h4>Belépés</h4>
            <?php
            if ($login_error_message != "") {
                echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
            }
            ?>
            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="">Felhasználónév/Email</label>
                    <input type="text" name="username" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="">Jelszó</label>
                    <input type="password" name="password" class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="submit" name="btnLogin" class="btn btn-primary" value="Belépés"/>
                </div>
            </form>
            <div class="form-group">
                Még nem regisztrált? <a href="registration.php">Regisztráció</a>
            </div>
        </div>
    </div>
</div>
<hr>
</body>
</html>