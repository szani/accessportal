Beléptetőrendszer 1.0

AZ ALKALMAZÁS MŰKÖDÖSÉNÉK LEÍRÁSA

Felhasználói beléptető portál admin és user jogosultságokkal. A program a telepítés és beállítások menüben egy többlépcsős telepítés során bekéri az adatbázis létrehozáshoz szükséges adatokat, 
létrehozza az adatbázist, majd az admin regisztráció menüben az első admin jogú felhasználót. A továbbiakban az oldal látogatói kétlépcsős azonosítási rendszeren keresztül regisztrálhatnak,
illetve léphetnek be az oldalra. A regisztráció (és a további beléptetések) érvényesítését a Google Hitelesítő applikációja telefonon/tableten keresztül végzi, 
ami a Google Play áruházból szabadon letölthető. Admin jogú felhasználóként lehetőség van a felhasználókat bannolni, törölni, illetve admin jogosultsággal ellátni. 
A beléptető portál megfelő használatához elengedhetetlen a Sütik használatának engedélyezése. Kérem engedélyezze ezen sütik jososultságát a böngészőjében.


Fejlesztői környezet

MySQL
Szerver verzió: 5.5.57-0ubuntu0.14.04.1 - (Ubuntu),
Apache/2.4.7 (Ubuntu),
phpMyAdmin Verziószám: 4.0.10deb1,

Adazbázisnév: accessportal,
Adatbázis elérhetőség (username: root, jelszó: ),
Online elérhetőség (username: stabbia, jelszó: vendeg1212)


Felhasznált technológia

Bootstrap 4,
PDO,
GoogleAuthenticator API,
GIT,
Javascript,
jQuery,
Ajax,
MySQLi

Fejlesztési terv: 

- PDO időzóna beállítása