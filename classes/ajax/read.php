<?php

define('TIMEZONE', 'Europe/Budapest');
date_default_timezone_set(TIMEZONE);

require 'lib.php';

$object = new CRUD();

// Design initial table header
$data = '<table class="table table-bordered table-striped table-responsive">
						<tr>
							<th>No.</th>
							<th>Név</th>
							<th>Felhasználónév</th>
							<th>Jogkör</th>
							<th>Eltiltva</th>
							<th>Regisztráció</th>
							<th>Bejelentkezve</th>
							<th>Módosítás</th>
							<th>Törlés</th>
						</tr>';


$users = $object->Read();

if (count($users) > 0) {
    $number = 1;
    foreach ($users as $user) {

    	
   if ($user['is_admin'] == "undefined" || $user['is_admin'] == "")  {
	  $setrole = "<span style = 'color: crimson;'>".'Állítsa be a jogosultságot!'."</span>";
	  }
   else $setrole = $user['is_admin'];

   if ($user['is_banned'] == "0" ) {
      $banned =  'nincs tiltva';
	  }
   else if ($user['is_banned'] == "1" ) {
      $banned =  'eltiltva';
	  }
   else $banned = "<span style = 'color: crimson;'>".'Állítsa be a hozzáférést!'."</span>";

        $data .= '<tr>
				<td>' . $number . '</td>
				<td>' . $user['name'] . '</td>
				<td>' . $user['username'] . '</td>
				<td>' . $setrole. '</td>
				<td>' . $banned. '</td>
				<td>' . $user['created_at'] . '</td>
				<td>' . $user['login_at'] . '</td>
				<td>
					<button onclick="GetUserDetails(' . $user['id'] . ')" class="btn btn-warning">Módosítás</button>
				</td>
				<td>
					<button onclick="DeleteUser(' . $user['id'] . ')" class="btn btn-danger">Törlés</button>
				</td>
    		</tr>';
        $number++;
    }
} else {
    // records not found
    $data .= '<tr><td colspan="6">Nem található bejegyzés!</td></tr>';
}

$data .= '</table>';

echo $data;

?>