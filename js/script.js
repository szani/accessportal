// Bejegyzés hozzárendelése Séma
/*function addRecord() {
    // értékek hozzárendelése
    var first_name = $("#first_name").val();
    first_name = first_name.trim();
    var last_name = $("#last_name").val();
    last_name = last_name.trim();
    var email = $("#email").val();
    email = email.trim();

    if (first_name == "") {
        alert("First name field is required!");
    }
    else if (last_name == "") {
        alert("Last name field is required!");
    }
    else if (email == "") {
        alert("Email field is required!");
    }
    else {
        // Add record
        $.post("ajax/create.php", {
            first_name: first_name,
            last_name: last_name,
            email: email
        }, function (data, status) {
            // close the popup
            $("#add_new_record_modal").modal("hide");

            // read records again
            readRecords();

            // clear fields from the popup
            $("#first_name").val("");
            $("#last_name").val("");
            $("#email").val("");
        });
    }
}*/
// READ : Bejegyzések kiolvasása
function readRecords() {
    $.get("classes/ajax/read.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}
function GetUserDetails(id) {
    // Felhasználó ID hozzáadása a rejtett mezőhöz
    $("#hidden_user_id").val(id);
    $.post("classes/ajax/details.php", {
            id: id
        },
        function (data, status) {
            // json data elemzése
            var user = JSON.parse(data);
            // A meglévő értékek beállítása a modális felbukkanó mezőkbe
            $("#update_name").val(user.name);
            $("#update_username").val(user.username);
            $("#update_email").val(user.email);
            $(".is_banned:checked").val(user.is_banned);
            $(".is_admin:checked").val(user.is_admin);
            $("#check7").val(user.check7);
            
        }
         
    );
    // A modális felugró ablak nyitása
            $("#update_user_modal").modal("show");
}


function UpdateUserDetails() {
    // értékek hozzárendelése
    var name = $("#update_name").val();
    name = name.trim();
    var username = $("#update_username").val();
    username = username.trim();
    var email = $("#update_email").val();
    email = email.trim();
    var is_admin = $(".is_admin:checked").val();
    var is_banned = $(".is_banned:checked").val();
    var check7 = $("#check7").val();


    if (name == "") {
        alert("Név kötelező!");
    }
    else if (username == "") {
        alert("Felhasználónév kötelező!");
    }
    else if (email == "") {
        alert("Email kötelező!!");
    }

    else {
        // rejtet mező értékének hozzárendelése
        var id = $("#hidden_user_id").val();

        // Ajax használatával frissítjük a rekordokat
        $.post("classes/ajax/update.php", {
                id: id,
                name: name,
                username: username,
                email: email,
                is_banned: is_banned,
                is_admin: is_admin,
                check7: check7
            },
            function (data, status) {
                // modális felugró ablak rejtése
                $("#update_user_modal").modal("hide");
                // Felhasználók újratöltése a readRecords() metódussal;
                readRecords();
            }
        );
    }
}

function DeleteUser(id) {
    var conf = confirm("Biztos benne, hogy törölni kívánja a felhasználót?");
    if (conf == true) {
        $.post("classes/ajax/delete.php", {
                id: id
            },
            function (data, status) {
                //  Felhasználók újratöltése a readRecords() metódussal;
                readRecords();
            }
        );
    }
}



$(document).ready(function () {
    // READ bejegyzések az oldal betöltésekor
    readRecords(); // hívás metódus
});

 $(document).ready(function(){  
      $('input[type="radio"]').click(function(){  
         //  var id = $(this).find("input[name=id]").val();
           var is_admin = $(this).find("input[name=is_admin]").val();
            var dataall = $(this).serialize();
     
           $.ajax({  
                url:"classes/ajax/update.php",  
                method:"POST",  
                data:dataall,
                success:function(data){  
                     $('#result').html(data);  
                }  
           });  
      });  
 });  
 


