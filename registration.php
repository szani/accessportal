<?php

// Start Session
session_start();
 $username = "";
 $password = "";
 
 if(isset($_POST['username'])){
  $username = $_POST['username'];
 }
 if (isset($_POST['password'])) {
  $password = $_POST['password'];
 }

// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);

require_once __DIR__ . '/classes/GoogleAuthenticator/GoogleAuthenticator.php';
$pga = new PHPGangsta_GoogleAuthenticator();
$secret = $pga->createSecret();

$register_error_message = '';

// check Register request
if (!empty($_POST['btnRegister'])) {
    if ($_POST['name'] == "") {
        $register_error_message = 'Név kötelező!';
    } else if ($_POST['email'] == "") {
        $register_error_message = 'Email cím kötelező!';
    } else if ($_POST['username'] == "") {
        $register_error_message = 'Felhasználónév kötelező!';
    } else if ($_POST['password'] == "") {
        $register_error_message = 'Jelszó kötelező!';
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $register_error_message = 'Érvénytelen email cím!';
    } else if ($app->isEmail($_POST['email'])) {
        $register_error_message = 'Email cím már foglalt!';
    } else if ($app->isUsername($_POST['username'])) {
        $register_error_message = 'Felhasználónév már foglalt!';
    } else {
        $user_id = $app->Register($_POST['name'], $_POST['email'], $_POST['username'], $_POST['password'], $secret);
        // set session and redirect user to the profile page
        $_SESSION['user_id'] = $user_id;
        header("Location: confirm_google_auth.php");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="template/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row jumbotron">
        <div class="col-md-12">
             <h1 style="text-align: center">
                Beléptetőrendszer regisztráció
             </h1>

        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 well">
            <h4>Regisztráció</h4>
            <?php
            if ($register_error_message != "") {
                echo '<div class="alert alert-danger"><strong>Hiba: </strong> ' . $register_error_message . '</div>';
            }
            ?>
            <form action="registration.php" method="post">
                <div class="form-group">
                    <label for="">Név</label>
                    <input type="text" name="name" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="">Felhasználónév</label>
                    <input type="text" name="username" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="">Jelszó</label>
                    <input type="password" name="password" class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="submit" name="btnRegister" class="btn btn-primary" value="Regisztráció"/>
                </div>
            </form>
            <div class="form-group">
                <!--Kattintson ide <a href="index.php">Belépés az oldalra</a>, ha már van regisztrációja.-->
            </div>
        </div>
    </div>

    <hr>
    
</div>

</body>
</html>

