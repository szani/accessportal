<?php
// Start Session
session_start();

// Database connection
require __DIR__ . '/config/db_connection.php';
 $db = DB();


// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);
?>
<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Beléptetőrendszer és rendszerbeállítások</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
    <div class="row jumbotron">
        <div class="col-md-12">
            <h1 style="text-align: center">
                Beléptetőrendszer Tájékoztató
            </h1>
        </div>
    </div>
    <div class="row">
      <div class="col-4">
        <div class="list-group" id="list-tab" role="tablist">
          <a class="list-group-item list-group-item-action active" id="list-description-list" data-toggle="list" href="#list-description" role="tab" aria-controls="description">1. A program leírása</a>
          <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">2. Telepítés és beállítások</a>
          <a class="list-group-item list-group-item-action" id="list-registrations-list" onclick="document.location.href='registration.php'" data-toggle="list" href="#list-registrations" role="tab" aria-controls="registrations">3. Admin regisztráció</a>
        </div>
      </div>
      <div class="col-8">
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="list-description" role="tabpanel" aria-labelledby="list-description-list">
              <h5 style="color: #007bff;text-transform: uppercase;">
                  Az alkalmazás működösénék leírása
              </h5>
              <br><br>
              <p>
                  <span style="color: #007bff;">Felhasználói beléptető portál</span> admin és user jogosultságokkal. 
                  A program a <strong>telepítés és beállítások</strong> menüben egy többlépcsős telepítés során bekéri az adatbázis létrehozáshoz szükséges adatokat, létrehozza az adatbázist, majd az <strong>admin regisztráció</strong> menüben az első admin jogú felhasználót.
                  A továbbiakban az oldal látogatói kétlépcsős azonosítási rendszeren keresztül regisztrálhatnak, illetve léphetnek be az oldalra. A regisztráció (és a további beléptetések) érvényesítését a <span style="color: crimson;"><strong>Google Hitelesítő</strong></span> applikációja telefonon/tableten keresztül végzi, ami a Google Play áruházból szabadon letölthető.
                  Admin jogú felhasználóként lehetőség van a felhasználókat bannolni, törölni, illetve admin jogosultsággal ellátni. A beléptető portál megfelő használatához elengedhetetlen a <span style="color: crimson;"><strong>Sütik</strong></span> használatának engedélyezése.
                  Kérem engedélyezze ezen sütik jososultságát a böngészőjében.
              </p>
              <h6>Fejlesztői környezet</h6>
              <ul>
                <li>Szerver verzió: 5.5.57-0ubuntu0.14.04.1 - (Ubuntu)</li>
                <li>Apache/2.4.7 (Ubuntu)</li>
                <li>phpMyAdmin Verziószám: 4.0.10deb1</li>
                <br>
                <li>Adazbázisnév: accessportal</li>
                <li><a href="https://crud-stabbia.c9users.io/phpmyadmin">Adatbázis elérhetőség</a> (username: root, jelszó: )</li>
                <li><a href="https://crud-stabbia.c9users.io/accessportal/index.php">Online elérhetőség</a> (username: stabbia, jelszó: vendeg1212)</li>
              </ul>
              <h6>Felhasznált technológia</h6>
              <ul>
                <li>Bootstrap 4</li>
                <li>PDO</li>
                <li>GoogleAuthenticator API</li>
                <li>GIT</li>
                <li>Javascript</li>
                <li>JQuery</li>
                <li>Ajax</li>
                <li>MySQLi</li>
            </ul>
         </div>
         <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
              <h5 style="color: crimson">
                  A program telepítéséhez és az adatbázis kapcsolat felállításhoz kérem kövesse a következő utasításokat!
              </h5>
              <br><br>
              <div>
              <h6>
                 1.  Kapcsolódás az adatbázishoz
              </h6>
              <br>
              <p>Töltse ki az űrlapot az adatbáziscsatlakozás létrehozásához!</p>
              <div>
                  <form class="connectForm" id="needs-validation" method="post" action="config/db_connection.php" novalidate>
                      <div class="form-group row">
                        <div class="col-md-6 mb-3">
                          <label for="validationServer">Szervernév</label>
                          <input type="text" class="form-control" id="servername" placeholder="Servername" value="localhost" name="servername" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="validationServer">Felhasználó neve</label>
                          <input type="text" class="form-control" id="dbusername" placeholder="stabbia" value="stabbia" name="dbusername" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-6 mb-3">
                          <label for="inputpassword3" class="col-sm-2 col-form-label">Jelszó</label>
                          <input type="password" class="form-control" id="dbpassword" placeholder="vendeg1212" name="dbpassword" value="vendeg1212" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="validationServer" class="col-sm-2 col-form-label">Adatbázisnév</label>
                          <input type="text" class="form-control" id="mydb" placeholder="accessportal" name="mydb" value="accessportal" required>
                       </div>
                      </div>
                    
                      <button class="btn btn-primary" type="submit">Kapcsolat létrehozása</button>
                  </form>
              </div>
            </div>
            <br><br>
             <h6>
                    2.  MySQL Adatbázistábla létrehozása
             </h6>
             <br>
             <p>Írja be a kívánt adatbázistábla elnevezést a következő mezőbe!</p>
              <div>
                  <form class="connectForm2" id="needs-validation" method="post" action="config/db_connection.php" novalidate>
                      <div class="form-group row">
                        <div class="col-md-6 mb-3">
                          <label for="validationServer">Táblanév</label>
                          <input type="text" class="form-control" id="tablename" placeholder="vezeteknev_keresztnev_Users" value="ivor_szaniszlo_Users" name="tablename" required>
                        </div>
                      </div>
                      <button class="btn btn-primary" type="submit"> Adattábla létrehozása</button>
                  </form>
              </div>
                <br><br>
                 <h6><i>Az adatbázis <strong>elérhető phpMyAdmin hozzáféréssel</strong> <a href="https://crud-stabbia.c9users.io/phpmyadmin">ITT</a> is! (username: root)</i></h6>
                <br>
              <div>
              <br><br><br><br>
          </div>
        </div>
      </div>
   </div>
 </div>
</body>
<!--
  //Adatok tárolása fájlban 
-->
<?php

/*
$data["servername"] = $_POST["servername"];
$data["dbusername"] =$_POST["dbusername"];
$data["dbpassword"] =$_POST["dbpassword"];
$data["mydb"] = $_POST["mydb"];

file_put_contents("savedata.txt", serialize($data)); 
*/
?>

<script>

// Example starter JavaScript for disabling form submissions if there are invalid fields

(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();

  //Registration button
  
$(document).ready(function() {
$('#btn-next').on('click', function() {
$('#loginbox').hide(); $('#1box').show();
   })
});

  // Local Storage
/*
var dataObject = {};

// Put the object into storage
localStorage.setItem('dataObject', JSON.stringify(dataObject));

// Retrieve the object from storage
var retrievedObject = localStorage.getItem('dataObject');

console.log('retrievedObject: ', JSON.parse(retrievedObject));
*/

var servername = $('#servername').val();
var dbusername = $('#dbusername').val();
var dbpassword = $('#dbpassword').val();
var mydb = $('#mydb').val();
var tablename = $('#tablename').val();

   
  $(function localStorage() {

   if (localStorage.chkbx && localStorage.chkbx != '') {
       $('#servername').attr('checked', 'checked');
       $('#dbusername').val(localStorage.dbusername);
       $('#dbpassword').val(localStorage.dbpassword);
       $('#mydb').val(localStorage.mydb);
   } else {
       $('#servername').removeAttr('checked');
       $('#dbusername').val('');
       $('#dbpassword').val('');
       $('#mydb').val('');
   }

   $('#servername').click(function() {

       if ($('#servername').is(':checked')) {
           // save dbusername and dbpassword and db 
           localStorage.dbusername = $('#dbusername').val();
           localStorage.dbpassword = $('#dbpassword').val();
           localStorage.mydb = $('#mydb').val();
           localStorage.chkbx = $('#servername').val();
       } else {
           localStorage.dbusername = '';
           localStorage.dbpassword = '';
           localStorage.mydb = '';
           localStorage.chkbx = '';
       }
   });
});



    </script>
</html>
         

