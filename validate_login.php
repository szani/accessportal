<?php
// Start Session
session_start();

 $username = "";
 $password = "";
 $is_banned ="";
 
 if(isset($_POST['username'])){
  $username = $_POST['username'];
 }
 if (isset($_POST['password'])) {
  $password = $_POST['password'];
 }

 
// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/classes/library/library.php';
$app = new DemoLib($db);
$user = $app->UserDetails($_SESSION['user_id']);
$id = $user->id;
$username = $user->username;
$userrole = $user->is_admin;
$is_banned = $user->is_banned;


require_once __DIR__ . '/classes/GoogleAuthenticator/GoogleAuthenticator.php';
$pga = new PHPGangsta_GoogleAuthenticator();

$error_message = '';

//Sessionkezelés
 session_regenerate_id();
  $_SESSION['sess_user_id'] = $id;
  $_SESSION['sess_username'] = $username;
  $_SESSION['sess_userrole'] = $userrole;
  $_SESSION['sess_is_banned'] = $is_banned;

  session_write_close();

if (isset($_POST['btnValidate']) ) {

    $code = $_POST['code'];
   
    if ($code == "") {
        $error_message = 'Kérjük, érvényesítse a hitelesítési kódot!';
    }
    else
    {   
         if($pga->verifyCode($user->google_secret_code, $code, 2) && $_SESSION['sess_userrole'] == "admin" && $_SESSION['sess_is_banned'] != "1" )
        {
            // sikeres
            $user_id = $app->LoginTime($login_at, $id); 
            header('Location: admin_profile.php');
             
        }
           elseif ($pga->verifyCode($user->google_secret_code, $code, 2) &&  $_SESSION['sess_userrole'] =="user" && $_SESSION['sess_is_banned'] != "1")
        {
            // sikeres
            $user_id = $app->LoginTime($login_at, $id); 
            header('Location: profile.php');
        }
           elseif ($pga->verifyCode($user->google_secret_code, $code, 2) &&  $_SESSION['sess_userrole'] =="undefined" && $_SESSION['sess_is_banned'] != "1")
        {
            // sikeres
            $user_id = $app->LoginTime($login_at, $id); 
            header('Location: admin_profile.php');
        }
           elseif ($_SESSION['sess_is_banned'] == "1")
        {
            // hiba
              
             $error_message = 'Tiltott felhasználó!';
                    echo '<div class="alert alert-danger"><strong>Hiba:  </strong> ' . $error_message . '</div>';
                     header('Location: index.php?err=3');
        }
    
        else
        {
            // hiba
            $error_message = 'A hitelesítő kód érvénytelen!';
        }
    }
}



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belépés hitelesítése</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="template/css/bootstrap.min.css">
</head>
<body>

<div class="container">
     <div class="row jumbotron">
        <div class="col-md-12">
             <h1 style="text-align: center">
                Google hitelesítés
             </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 well">
            <h4>Kétlépcsős hitelesítés</h4>
            <form method="post" action="validate_login.php">
                <?php
                if ($error_message != "") {
                    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $error_message . '</div>';
                }
                ?>
                <div class="form-group">
                    <input type="text" name="code" placeholder="Írja be a hitelesítési kódot" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" name="btnValidate" class="btn btn-primary">Érvényesítés</button>
                </div>
            </form>

            <div class="form-group">
                Kattintson ide <a href="index.php">Belépés az oldalra</a>, ha már van regisztrációja.
            </div>
        </div>
    </div>

    <hr>
    
</div>

</body>
</html>